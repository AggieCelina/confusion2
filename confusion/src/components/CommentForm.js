import React from 'react';
import Button from 'react-bootstrap/Button';
import { Control, LocalForm, Errors } from 'react-redux-form'
import { Modal, ModalHeader, ModalBody, Label, Row } from 'reactstrap';

const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);


class CommentForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            isModalOpen: false
        }
        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleSubmit(values) {
        this.toggleModal();
        alert("Current State is: " + JSON.stringify(values))
    }

    render() {
        return (
            <div>
                <Button variant="outline-secondary" color="primary" onClick={this.toggleModal}>
                    <i className="fa fa-pencil fa-lg"></i> Submit Comment
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group px-3">
                                <Label htmlFor="rating">Rating</Label>
                                <Control.select model=".rating" id="rating" name="rating"
                                    className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Row>
                            <Row className="form-group px-3">
                                <Label htmlFor="user_name">Your Name</Label>
                                <Control.text model=".user_name" id="user_name" name="user_name"
                                    placeholder="Your Name"
                                    className="form-control"
                                    validators={{
                                        minLength: minLength(2), maxLength: maxLength(15)
                                    }}
                                />
                                <Errors
                                    className="text-danger"
                                    model=".user_name"
                                    show="touched"
                                    messages={{
                                        minLength: 'Must be greater than 2 characters. ',
                                        maxLength: 'Must be 15 characters or less. '
                                    }}
                                />
                            </Row>
                            <Row className="form-group px-3">
                                <Label htmlFor="user_name">Comment</Label>
                                <Control.textarea model=".comment" id="comment" name="comment" rows="8"
                                    className="form-control"
                                />
                            </Row>
                            <Row className="form-group px-3">
                                <Button type="submit" value="submit" color="primary">Submit</Button>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal> 
            </div>
        );
    }
}

export default CommentForm;